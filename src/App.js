import './App.css';
import Header from './components/layout/Header';
import Footer from './components/layout/Footer';
import ContentLeft from './components/layout/ContentLeft';
import Detail from './components/blog/Detail';
import { useLocation } from 'react-router-dom';
import MenuUser from './components/layout/MenuUser';
import { UserProvider } from './UserContext';
import { CartContext } from './CartContext';
import { useState } from 'react';




function App(props) {
  const [getTong,setTong] = useState();
  let params1 = useLocation();
  function getCartContext(data){
    setTong(data)
  }
  let user = {
    name:'Tuan Anh'
  }
  return (
    <>
      <CartContext.Provider value={{
      user:user,
      getCartContext:getCartContext,
      getTong:getTong
      }}>
        <Header />
      <section>
        <div className='container'>
          <div className='row'>
            {params1['pathname'].includes("user") ? <MenuUser /> : <ContentLeft />}
            {props.children}
          </div>
        </div>
      </section>
      <Footer />
      </CartContext.Provider>
    </>

  );
}

export default App;
