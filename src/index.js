import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
import Detail from './components/blog/Detail';
import Blog from './components/blog/Blog';
import Register from './components/blog/Register';
import Login from './components/blog/Login';
import Home from './components/layout/Home';
import UpdateUser from './components/blog/UpdateUser';
import AddProduct from './components/product/AddProduct';
import ListProduct from './components/product/ListProduct';
import UpdateProduct from './components/product/UpdateProduct';
import ProductDetail from './components/product/ProductDetail';
import Cart from './components/product/Cart';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <React.StrictMode>
    <Router>
      <App>
        <Routes>
          <Route index path='/home' element={<Home />} />
          <Route index path='/blog/list' element={<Blog />} />
          <Route path='/blog/detail/:id' element={<Detail />} />
          <Route path='/register' element={<Register />} />
          <Route path='/login' element={<Login />} />
          <Route path='/user' element={<UpdateUser />} />
          <Route path='/product/add' element={<AddProduct />} />
          <Route path='/product/list' element={<ListProduct />} />
          <Route path='/product/update/:id' element={<UpdateProduct />} />
          <Route path='/product/detail/:id' element={<ProductDetail />} />
          <Route path='/product/cart' element={<Cart />} />
        </Routes>
      </App>
    </Router>
  </React.StrictMode>
);
reportWebVitals();
