import axios from "axios";
import { useEffect, useState } from "react";

function AddProduct() {
    const [getInput, setInput] = useState({
        name: "",
        price: "",
        category: "",
        brand: "",
        sale: "",
        status: "",
        profile: "",
        detail: "",
    })
    const handleInput = (e) => {
        const nameInput = e.target.name;
        const value = e.target.value;
        setInput(state => ({ ...state, [nameInput]: value }));
    }
    const [getCategory, setCategory] = useState([]);
    const [getBrand, setBrand] = useState([]);
    useEffect(() => {
        axios.get("http://localhost:8080/laravel/public/api/category-brand")
            .then(res => {
                setBrand(res.data.brand)
                setCategory(res.data.category)
            })
            .catch(error => console.log(error))
    }, [])
    function renderCategory() {
        if (getCategory.length > 0) {
            return getCategory.map((value, key) => {
                return (
                    <option name="category" onChange={handleInput} key={key} value={value.id}>{value.category}</option>
                );
            })
        }
    }
    function renderBrand() {
        if (getBrand.length > 0) {
            return getBrand.map((value, key) => {
                return (
                    <option name="brand" key={key} value={value.id}>{value.brand}</option>
                );
            })
        }
    }
    function toggle() {
        if (getInput.status == "0")
            return (
                <input onChange={handleInput} name="sale" placeholder="Sale %"></input>
            )
    }
    const [getErr, setErr] = useState({})
    function renderErrors() {
        if (Object.keys(getErr).length > 0) {
            return Object.keys(getErr).map((key, index) => {
                return (
                    <li key={index}>{getErr[key]}</li>
                );
            })
        }
    }
    const [getFile, setFile] = useState("");
    function hanldeFile(e){
        setFile(e.target.files)
    }
    function hanldeForm(e) {
        e.preventDefault();
        let errorsSubmit = {};
        const img = ["jpeg","jpg","png"]
        let flag = true;

        if (getInput.name == "") {
            errorsSubmit.name = "vui lòng nhập tên sản phẩm";
            flag = false;
        }
        if (getInput.price == "") {
            errorsSubmit.price = "vui lòng nhập giá";
            flag = false;
        }
        if (getInput.profile == "") {
            errorsSubmit.profile = "vui lòng nhập profile";
            flag = false;
        }
        if(getInput.status == ""){
            errorsSubmit.status = "vui lòng chọn sale hoặc new";
            flag = false;
        }else if(getInput.status == "0" && getInput.sale ==""){
            errorsSubmit.sale = "vui lòng nhập nummber sale";
            flag = false;
        }
        if (getInput.detail == "") {
            errorsSubmit.detail = "vui lòng nhập detail";
            flag = false;
        }
        if(getFile == ""){
            errorsSubmit.file = "vui long chon anh"
            flag = false;
        }else if(getFile.length > 3){
            errorsSubmit.file = "vui long chon toi da 3 anh"
            flag = false;
        }else{
            const nameFile = getFile[0].name.split(".",3);
            const nameFiles = nameFile[1]
            if(getFile[0].size > 1024*1024){
                errorsSubmit.file = "anh lon hon 1mb"
                flag = false;
            }else if(img.includes(nameFiles) == false){
                errorsSubmit.file = "anh khong dung dinh dang"
                flag = false;
            }
        }
        if (!flag) {
            setErr(errorsSubmit);
        }else{
            let localLogin = localStorage.getItem("Login")
            let localToken = localStorage.getItem("Token")
            if (localLogin && localToken) {
                localLogin = JSON.parse(localLogin)
                localToken = JSON.parse(localToken)
            }
            let url = 'http://localhost:8080/laravel/public/api/user/add-product';
            let accessToken = localToken.token;
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            const formData = new FormData();
            formData.append('name', getInput.name);
            formData.append('price', getInput.price);
            formData.append('category',getInput.category);
            formData.append('brand', getInput.brand);
            formData.append('status', getInput.status);
            formData.append('sale', getInput.sale? getInput.sale : 0);
            formData.append('company', getInput.profile);
            formData.append('detail', getInput.detail);
            Object.keys(getFile).map((item,i)=>{
                formData.append("file[]",getFile[item])
            })
            axios.post(url, formData, config)
                .then(response => {
                    if(response.data.errors){
                        setErr(response.data.errors)
                    }else{
                        alert("add thanh cong")
                    }
                });
        }
    }
    return (
        <section id="form">
            <div className="signup-form">
                <h2>Add Product</h2>
                <form encType="multipart/form-data" onSubmit={hanldeForm}>
                    {renderErrors()}
                    <input type="text" name="name" placeholder="Name" onChange={handleInput} />
                    <input type="text" name="price" placeholder="Price" onChange={handleInput} />
                    <select name="category" onChange={handleInput}>
                        {renderCategory()}
                    </select>
                    <select name="brand" onChange={handleInput}>
                        {renderBrand()}
                    </select>
                    <select name="status" onChange={handleInput}>
                        <option name="status" value="">Choose</option>
                        <option name="status" value="1">New</option>
                        <option name="status" value="0">Sale</option>
                    </select>
                    {toggle()}
                    <input type="text" name="profile" placeholder="Company profile" onChange={handleInput} />
                    <input type="file" name="files" placeholder="" onChange={hanldeFile} multiple />
                    <textarea type="text" name="detail" placeholder="Detail" onChange={handleInput}></textarea>
                    <button type="submit" className="btn btn-default">Add</button>
                </form>
            </div>
        </section>
    );
}

export default AddProduct;