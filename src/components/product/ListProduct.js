import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function ListProduct() {
    const [getData, setData] = useState([]);
    useEffect(() => {

        let localLogin = localStorage.getItem("Login")
        let localToken = localStorage.getItem("Token")
        if (localLogin && localToken) {
            localLogin = JSON.parse(localLogin)
            localToken = JSON.parse(localToken)
        }
        let accessToken = localToken.token;
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            }
        };
        let url = 'http://localhost:8080/laravel/public/api/user/my-product';
        axios.get(url, config)
            .then(response => {
                setData(response.data.data)
            });
    }, [])
    function DeleteProduct(e) {
        const id_product = e.target.id
        
        if(id_product.length > 0){
            let localLogin = localStorage.getItem("Login")
            let localToken = localStorage.getItem("Token")
            if (localLogin && localToken) {
                localLogin = JSON.parse(localLogin)
                localToken = JSON.parse(localToken)
            }
            let accessToken = localToken.token;
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            axios.get("http://localhost:8080/laravel/public/api/user/delete-product/" + id_product,config)
            .then(res => {
                setData(res.data.data)
                alert("Deleted")
            })
            .catch(error => console.log(error))
        }
       
        
    }
    function renderProduct() {

        if (Object.keys(getData).length > 0) {
            return Object.keys(getData).map((key, value) => {
                let imageProduct = getData[key].image
                if (imageProduct.length > 0) {
                    imageProduct = JSON.parse(imageProduct)
                }
                return (
                    <tbody>
                    <tr key={key}>
                        <th>{getData[key].id}</th>
                        <th>{getData[key].name}</th>
                        {/* <th>{imageProduct[0]}</th> */}
                        <th><img className="image_product" src={"http://localhost:8080/laravel/public/upload/user/product/" + getData[key].id_user + "/" + imageProduct[0]}></img></th>
                        <th>{getData[key].price}</th>
                        <th>
                            <a id={getData[key].id} onClick={DeleteProduct}>Delect</a>
                            <Link id={getData[key].id} to={"/product/update/" + getData[key].id}>Update</Link>
                        </th>
                    </tr>
                </tbody>
                );
            })
        }
    }
    return (
        <div>
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
                {renderProduct()}
            </table>
        </div>
        
    );
}

export default ListProduct;