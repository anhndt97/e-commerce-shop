import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

function UpdateProduct() {
    let params = useParams();
    const [getInput, setInput] = useState({
        name: "",
        price: "",
        category: "",
        brand: "",
        sale: "",
        status: "",
        profile: "",
        detail: "",
    })
    const [getMessages,setMessages] = useState([]);
    const [getErr, setErr] = useState({})
    const [getFile, setFile] = useState("");
    const [getCategory, setCategory] = useState([]);
    const [getBrand, setBrand] = useState([]);
    const [getData, setData] = useState([]);

    useEffect(() => {
        axios.get("http://localhost:8080/laravel/public/api/category-brand")
            .then(res => {
                setBrand(res.data.brand)
                setCategory(res.data.category)
            })
            .catch(error => console.log(error))
    }, [])
    const handleInput = (e) => {
        const nameInput = e.target.name;
        const value = e.target.value;
        setInput(state => ({ ...state, [nameInput]: value }));
    }
    useEffect(() => {
        let localLogin = localStorage.getItem("Login")
        let localToken = localStorage.getItem("Token")
        if (localLogin && localToken) {
            localLogin = JSON.parse(localLogin)
            localToken = JSON.parse(localToken)
        }
        let accessToken = localToken.token;
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            }
        };
        axios.get("http://localhost:8080/laravel/public/api/user/product/" + params.id, config)
            .then(res => {
                setInput({
                    name: res.data.data.name,
                    price: res.data.data.price,
                    category: res.data.data.category,
                    brand: res.data.data.brand,
                    sale: res.data.data.sale,
                    status: res.data.data.status,
                    profile: res.data.data.company_profile,
                    detail: res.data.data.detail
                })
                setData(res.data.data)
            })
            .catch(error => console.log(error))
    }, [])
    function hanldeFile(e) {
        setFile(e.target.files)
    }
    function toggle() {
        if (getInput.status == "0")
            return (
                <input onChange={handleInput} name="sale" placeholder="Sale %"></input>
            )
    }
    function addMessage(e){
        let xx = e.target.value;
        let checked = e.target.checked
        if(checked){
            setMessages(oldMessages => [xx,...oldMessages])
        }else{
            if(getMessages.includes(xx) == true){
                setMessages(getMessages.splice(xx,getMessages),1)
            }
        }
    }
    console.log(getMessages)
    function renderCategory() {
        if (getCategory.length > 0) {
            return getCategory.map((value, key) => {
                return (
                    <option name="category" onChange={handleInput} key={key} value={value.id}>{value.category}</option>
                );
            })
        }
    }
    function renderBrand() {
        if (getBrand.length > 0) {
            return getBrand.map((value, key) => {
                return (
                    <option name="brand" key={key} value={value.id}>{value.brand}</option>
                );
            })
        }
    }

    function renderImg(){  
        if (Object.keys(getData).length > 0) {
            return getData.image.map((value, key) => {
                return (
                    <li key={key}>
                        <img className="image_product" src={"http://localhost:8080/laravel/public/upload/user/product/" + getData.id_user + "/" + value}></img>
                        <input type="checkbox" onClick={addMessage} value={value}></input>
                    </li>
                );
            })
        }
    }
    function renderErrors() {
        if (Object.keys(getErr).length > 0) {
            return Object.keys(getErr).map((key, index) => {
                return (
                    <li key={index}>{getErr[key]}</li>
                );
            })
        }
    }
    function hanldeForm(e) {
        e.preventDefault();
        let errorsSubmit = {};
        const img = ["jpeg", "jpg", "png"]
        let flag = true;

        if (getInput.name == "") {
            errorsSubmit.name = "vui lòng nhập tên sản phẩm";
            flag = false;
        }
        if (getInput.price == "") {
            errorsSubmit.price = "vui lòng nhập giá";
            flag = false;
        }
        if (getInput.profile == "") {
            errorsSubmit.profile = "vui lòng nhập profile";
            flag = false;
        }
        if (getInput.status == "") {
            errorsSubmit.status = "vui lòng chọn sale hoặc new";
            flag = false;
        } else if (getInput.status == "0" && getInput.sale == "") {
            errorsSubmit.sale = "vui lòng nhập nummber sale";
            flag = false;
        }
        if (getInput.detail == "") {
            errorsSubmit.detail = "vui lòng nhập detail";
            flag = false;
        }
        if (getFile == "") {
            errorsSubmit.file = "vui long chon anh"
            flag = false;
        } else if (getFile.length > 3) {
            errorsSubmit.file = "vui long chon toi da 3 anh"
            flag = false;
        } else {
            const nameFile = getFile[0].name.split(".", 3);
            const nameFiles = nameFile[1]
            if (getFile[0].size > 1024 * 1024) {
                errorsSubmit.file = "anh lon hon 1mb"
                flag = false;
            } else if (img.includes(nameFiles) == false) {
                errorsSubmit.file = "anh khong dung dinh dang"
                flag = false;
            }
        }
        console.log(getInput.profile)
        if (!flag) {
            setErr(errorsSubmit);
        }else{
            let localLogin = localStorage.getItem("Login")
            let localToken = localStorage.getItem("Token")
            if (localLogin && localToken) {
                localLogin = JSON.parse(localLogin)
                localToken = JSON.parse(localToken)
            }
            let url = 'http://localhost:8080/laravel/public/api/user/edit-product/' + params.id;
            let accessToken = localToken.token;
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            const formData = new FormData();
            formData.append('name', getInput.name);
            formData.append('price', getInput.price);
            formData.append('category',getInput.category);
            formData.append('brand', getInput.brand);
            formData.append('status', getInput.status);
            formData.append('sale', getInput.sale? getInput.sale : 0);
            formData.append('company', getInput.profile);
            formData.append('detail', getInput.detail);
            Object.keys(getFile).map((item,i)=>{
                formData.append("file[]",getFile[item])
            })
            Object.keys(getMessages).map((item,i)=>{
                formData.append("avatarCheckBox[]",getMessages[item])
            })
            axios.post(url, formData, config)
                .then(response => {
                    if(response.data.errors){
                        setErr(response.data.errors)
                    }else{
                        alert("Update thanh cong")
                    }
                });
        }
    }
   
    return (
        <section id="form">
            <div className="signup-form">
                <h2>Edit Product</h2>
                <form encType="multipart/form-data" onSubmit={hanldeForm}>
                    {renderErrors()}
                    <input type="text" name="name" value={getInput.name} onChange={handleInput} />
                    <input type="text" name="price" value={getInput.price} onChange={handleInput} />
                    <select name="category" onChange={handleInput}>
                        {renderCategory()}
                    </select>
                    <select name="brand" onChange={handleInput}>
                        {renderBrand()}
                    </select>
                    <select name="status" onChange={handleInput}>
                        <option name="status" value="">Choose</option>
                        <option name="status" value="1">New</option>
                        <option name="status" value="0">Sale</option>
                    </select>
                    {toggle()}
                    <input type="text" name="profile" value={getInput.profile} onChange={handleInput} />
                    <input type="file" name="files" placeholder="" onChange={hanldeFile} multiple />
                    <ul>
                    {renderImg()}
                    </ul>
                    <textarea type="text" name="detail" value={getInput.detail} onChange={handleInput}></textarea>
                    <button type="submit" className="btn btn-default">Submit</button>
                </form>
            </div>
        </section>
    );
}

export default UpdateProduct;