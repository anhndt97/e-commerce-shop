import axios from "axios"
import { useContext, useEffect, useState } from "react"
import App from "../../App"
import { CartContext } from "../../CartContext"

function Cart() {
    const value = useContext(CartContext)
    let tong = 0
    let objcha = {}
    let product = localStorage.getItem("Cart")
    if (product) {
        product = JSON.parse(product)
    }
    const [getData, setData] = useState([])
    const [getCart, setCart] = useState([])
    useEffect(() => {
        let url = 'http://localhost:8080/laravel/public/api/product/cart';
        axios.post(url, product)
            .then(response => {
                setData(response.data.data)
                setCart(response.data.data.length)
            });
    }, [])
    value.getCartContext(getCart)
    getData.map((value, key) => {
        tong = tong + getData[key].price * getData[key].qty

    })
    
    
    function Add(e) {
        let idpd = e.target.id
        let newData = [...getData]
        Object.keys(newData).map((key, value) => {
            if (idpd == newData[key].id) {
                newData[key].qty += 1
            }
        })
        setData(newData)
        let objcon = localStorage.getItem("Cart")
        if (objcon) {
            objcha = JSON.parse(objcon)
            Object.keys(objcha).map((key, value) => {
                if (idpd == key) {
                    objcha[key] += 1
                }
            })
        }
        localStorage.setItem("Cart", JSON.stringify(objcha))

    }
    function Minus(e) {
        let idpd = e.target.id
        let qty = e.target.name
        let newData = [...getData]
        Object.keys(newData).map((key, value) => {
            if (idpd == newData[key].id) {
                newData[key].qty -= 1
            }
        })
        setData(newData)
        if (parseInt(qty) <= 1) {
            Object.keys(newData).map((key, value) => {
                if (idpd == newData[key].id) {
                    delete newData[key]
                }
            })
            let newArray = newData.filter(function (v) { return v !== '' });
            setData(newArray);
            let objcon1 = localStorage.getItem("Cart")
            if (objcon1) {
                objcha = JSON.parse(objcon1)
                Object.keys(objcha).map((key, value) => {
                    if (value == 0) {
                        delete objcha[key]
                    }
                })
                localStorage.setItem("Cart", JSON.stringify(objcha))
            }
        }
    }

    function Delete(e) {
        let idpd = e.target.id
        let newDataa = [...getData]
        Object.keys(newDataa).map((key, value) => {
            if (idpd == newDataa[key].id) {
                delete newDataa[key]
            }
        })
        let newArray = newDataa.filter(function (v) { return v !== '' });
        setData(newArray);
        let objcon = localStorage.getItem("Cart")
        if (objcon) {
            objcha = JSON.parse(objcon)
            Object.keys(objcha).map((key, value) => {
                if (idpd == key) {
                    delete objcha[key]
                }
            })
            localStorage.setItem("Cart", JSON.stringify(objcha))
        }
    }

    function renderData() {
        if (Object.keys(getData).length > 0) {
            return Object.keys(getData).map((value, key) => {
                let imageProduct = getData[value].image
                let total = getData[value].price * getData[value].qty
                if (imageProduct.length > 0) {
                    imageProduct = JSON.parse(imageProduct)
                }
                return (
                    <tbody key={key}>
                        <tr className="trtable">
                            <td className="cart_product">
                                <img src={"http://localhost:8080/laravel/public/upload/user/product/" + getData[value].id_user + "/" + imageProduct[0]}></img>
                            </td>
                            <td className="cart_description">
                                <h4><a href="">{getData[value].name}</a></h4>
                                <p>Web ID: 1089772</p>
                            </td>
                            <td className="cart_price">
                                <p>{getData[value].price}</p>
                            </td>
                            <td className="cart_quantity">
                                <div className="cart_quantity_button">
                                    <a onClick={Add} id={getData[value].id} name={getData[value].qty} className="cart_quantity_up" href> + </a>
                                    <input className="cart_quantity_input" type="text" name="quantity" value={getData[value].qty} autocomplete="off" size="2" />
                                    <a onClick={Minus} id={getData[value].id} name={getData[value].qty} className="cart_quantity_down" href> - </a>
                                </div>
                            </td>
                            <td className="cart_total">
                                <p className="cart_total_price">{total}</p>
                            </td>
                            <td className="cart_delete">
                                <a onClick={Delete} id={getData[value].id} className="cart_quantity_delete" href><i className="fa fa-times"></i></a>
                            </td>
                        </tr>
                    </tbody>
                )
            })
        }
    }
    return (
        <div>
            <table className="cart">
                <thead>
                    <tr className="">
                        <th className="image">Image</th>
                        <td class="description">Name</td>
                        <th className="price">Price</th>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                    </tr>
                </thead>
                {renderData()}
                <div class="total_area">
                    <ul>
                        <li>Total <span class="total_a"></span></li>
                    </ul>
                    <a class="btn btn-default check_out" href="">{tong}</a>
                </div>
            </table>
        </div>
    );
}

export default Cart;