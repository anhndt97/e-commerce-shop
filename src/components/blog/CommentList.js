import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Detail from "./Detail";

function CommentList(props) {
    let params = useParams();
    const getlist = props.getlist
    function handleRepply(e) {
        props.getidCmt(e.target.id)
    }
    function renderData() {
        if (getlist.length > 0) {
            return getlist.map((value, key) => {
                if (value.id_comment == 0) {
                    return (
                        <React.Fragment key={key}>
                            <li className="media">
                                <a className="pull-left" href="#">
                                    <img className="media-object" src={"http://localhost:8080/laravel/public/upload/user/avatar/" + value.image_user} alt="" />
                                </a>
                                <div className="media-body">
                                    <ul className="sinlge-post-meta">
                                        <li><i className="fa fa-user" />{value.name_user}</li>
                                        <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                                        <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                                    </ul>
                                    <p>{value.comment}</p>
                                    <a id={value.id} className="btn btn-primary" onClick={handleRepply} href><i className="fa fa-reply" />Replay</a>
                                </div>
                            </li>
                            {getlist.map((value2, j) => {
                                if (value.id == value2.id_comment) {
                                    return (
                                        <li key={j} index={j} className="media second-media">
                                            <a className="pull-left" href="#">
                                                <img className="media-object" src="images/blog/man-three.jpg" alt="" />
                                            </a>
                                            <div className="media-body">
                                                <ul className="sinlge-post-meta">
                                                    <li><i className="fa fa-user" />{value2.name_user}</li>
                                                    <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                                                    <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                                                </ul>
                                                <p>{value2.comment}</p>
                                                <a className="btn btn-primary" href><i className="fa fa-reply" />Replay</a>
                                            </div>
                                        </li>
                                    )
                                }
                            })}
                        </React.Fragment>
                    );
                }
            })
        }
    }
    return (
        <div className="response-area">
            <h2>3 RESPONSES</h2>
            <ul class="media-list">
                {renderData()}
            </ul>
        </div>
    );
}

export default CommentList;