import axios from "axios";
import { useState } from "react";
import {useNavigate} from "react-router-dom"

function Login() {
    const navigate = useNavigate();
    const [getInput, setInput] = useState({
        email:"",
        pass:"",
        level:"",
    });
    const [getErr, setErr] = useState({})
    const handleInput = (e) => {
        const nameInput = e.target.name;
        const value = e.target.value;
        setInput(state => ({ ...state, [nameInput]: value }))
    }
    function hanldeForm(e) {
        e.preventDefault();
        let errorsSubmit = {};
        const regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        let flag = true;
        if(getInput.email == ""){
            errorsSubmit.email = "vui long nhap email";
            flag = false;
        }else if(!regex.test(getInput.email)){
            errorsSubmit.email = "sai dinh dang, vd(abc@gmail.com.vn)"
            flag = false;
        }
        if(getInput.pass == ""){
            errorsSubmit.pass = "vui long nhap pass"
            flag = false;
        }
        if (!flag) {
            setErr(errorsSubmit);    
        }else{
            let obj = {}
            let obj1 = {}
            const data ={
                email: getInput.email,
                password: getInput.pass,
                level: 0,
            }
            const obj2 = getInput.pass
            setErr({})
            const success = {}
            axios.post("http://localhost:8080/laravel/public/api/login",data)
            .then(res=>{
                if(res.data.errors){
                    setErr(res.data.errors)
                }else{
                    obj = res.data.Auth
                    obj1 = res.data.success
                    localStorage.setItem("Login",JSON.stringify(obj))
                    localStorage.setItem("Token",JSON.stringify(obj1))
                    localStorage.setItem("Password",JSON.stringify(obj2))
                    alert("Login success!")
                    navigate('/home');
                    console.log(res.data)
                }
            })
        }
    }
    function renderErrors() {
        if (Object.keys(getErr).length > 0) {
            return Object.keys(getErr).map((key, index) => {
                return (
                    <li key={index}>{getErr[key]}</li>
                )
            })
        }
    }
    return (
        <section id="form">
            <div className="container">
                <div className="row">
                    <div className="col-sm-4 col-sm-offset-1">
                        <div className="login-form">{/*login form*/}
                            <h2>Login to your account</h2>
                            <form onSubmit={hanldeForm}>
                                {renderErrors()}
                                <input type="text" placeholder="Email Address" name="email" onChange={handleInput}/>
                                <input type="password" placeholder="Password" name="pass" onChange={handleInput} />
                                <span>
                                    <input type="checkbox" className="checkbox" />
                                    Keep me signed in
                                </span>
                                <button type="submit" className="btn btn-default">Login</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Login;