import axios from "axios";
import { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import CommentList from "./CommentList";
import Comment from "./Comment";
import Rate from "./Rate";

function Detail() {
    let params = useParams();
    const [getdata, setData] = useState('');
    const [getlist ,setlist] = useState('')
    const [getid ,setid] = useState('')
    useEffect(() => {
        axios.get("http://localhost:8080/laravel/public/api/blog/detail/" + params.id)
            .then(res => {
                setData(res.data.data)
                setlist(res.data.data.comment)
            })
            .catch(error => console.log(error))
    }, [])
    function getCmt(x){  
        setlist(getlist.concat(x))
    }
    function getidCmt(a){  
        setid(a)
    }
    function renderData() {
        if (Object.keys(getdata).length > 0) {
            return (
                <div className="single-blog-post">
                    <h3>{getdata.title}</h3>
                    <a href>
                        <img src={"http://localhost:8080/laravel/public/upload/blog/image/" + getdata.image} alt="" />
                    </a>
                    <p>{getdata.content}</p> <br />
                    <p>
                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p> <br />
                    <p>
                        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p> <br />
                    <p>
                        Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                    </p>
                </div>
            )
        }
    }
    return (
        <div className="col-sm-9">
            <div className="blog-post-area">
                <h2 className="title text-center">Latest From our Blog</h2>
                {renderData()}
            </div>
            <Rate/>
            <div className="socials-share">
                <a href><img src="images/blog/socials.png" alt="" /></a>
            </div>
            <CommentList getlist={getlist}  getidCmt={getidCmt}/>
            <Comment getCmt = {getCmt} getid={getid} />
        </div>
    );
}

export default Detail;

