import axios from "axios";
import { useState } from "react";
import { useParams } from "react-router-dom";

function Comment(props) {
    let params = useParams();
    const [getErr, setErr] = useState({})
    const handleInput = (e) => {
        const nameInput = e.target.name;
        const value = e.target.value;
        setInput(state => ({ ...state, [nameInput]: value }))
    }
    const [getInput, setInput] = useState({
        message: ""
    });
    const getid= props.getid
    function renderErrors() {
        if (Object.keys(getErr).length > 0) {
            return Object.keys(getErr).map((key, index) => {
                return (
                    <li key={index}>{getErr[key]}</li>
                )
            })
        }
    }
    function hanldeForm(e) {
        e.preventDefault();
        let errorsSubmit = {};
        let flag = true;

        let local = localStorage.getItem("Login")

        if (local) {
            local = JSON.parse(local)
        }
        if (!local) {
            return (
                alert("Please Login!")
            );
        } else {
            if (getInput.message == "") {
                alert("vui long nhap comment")
                flag = false;
            }
        };
        if (!flag) {
            setErr(errorsSubmit)
        } else {
            let localLogin = localStorage.getItem("Login")
            let localToken = localStorage.getItem("Token")
            if (localLogin && localToken) {
                localLogin = JSON.parse(localLogin)
                localToken = JSON.parse(localToken)
            }
            let url = 'http://localhost:8080/laravel/public/api/blog/comment/' + params.id;
            let accessToken = localToken.token;
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            const formData = new FormData();
            formData.append('id_blog', params.id);
            formData.append('id_user', localLogin.id);
            formData.append('id_comment',getid? getid : 0);
            formData.append('comment', getInput.message);
            formData.append('image_user', localLogin.avatar);
            formData.append('name_user', localLogin.name);
            axios.post(url, formData, config)
                .then(response => {
                    console.log(response)
                    props.getCmt(response.data.data)
                    alert("comment thanh cong")
                });
        }
    }
    return ( 
        <div className="replay-box">
                <div className="row">
                    <div className="col-sm-12">
                        <h2>Leave a replay</h2>
                        <div className="text-area">
                            <div className="blank-arrow">
                                <label>Your Name</label>
                            </div>
                            <span>*</span>
                            <form onSubmit={hanldeForm}>
                                {renderErrors()}
                                <textarea name="message" rows={11} defaultValue={""} onChange={handleInput} />
                                <button type="submit" className="btn btn-primary">post comment</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
     );
}

export default Comment;