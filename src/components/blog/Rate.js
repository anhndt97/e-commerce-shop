import axios from 'axios';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import StarRatings from 'react-star-ratings';

function Rate() {
    let params = useParams();
    const [rating, setRating] = useState()
    useEffect(() => {
        axios.get("http://localhost:8080/laravel/public/api/blog/rate/" + params.id)
            .then(res => {
                let getRate = (res.data.data)
                if (getRate.length > 0) {
                    let totalRate = 0
                    let totalRates = 0;
                    Object.keys(getRate).map(function (value, key) {
                        totalRates = Object.keys(getRate).length
                        let ratee = (getRate[value].rate)
                        totalRate += ratee
                    })
                    let rateNumber = totalRate / totalRates
                    setRating(rateNumber)
                }
            })
            .catch(error => console.log(error))
    }, [])
    console.log(rating)
    function changeRating(newRating, name) {
        setRating(newRating)
        let local = localStorage.getItem("Login")
        if (local) {
            local = JSON.parse(local)
        }
        if (!local) {
            return (
                alert("Please Login!")
            );
        } else {
            let localLogin = localStorage.getItem("Login")
            let localToken = localStorage.getItem("Token")
            if (localLogin && localToken) {
                localLogin = JSON.parse(localLogin)
                localToken = JSON.parse(localToken)
            }
            let url = 'http://localhost:8080/laravel/public/api/blog/rate/' + params.id;
            let accessToken = localToken.token;
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            const formData = new FormData();
            formData.append('blog_id', params.id);
            formData.append('user_id', localLogin.id);
            formData.append('rate', newRating);
            axios.post(url, formData, config)
                .then(response => {
                });
        }
    }
    return (
        <div className="rating-area">
            <ul className="ratings">
                <StarRatings
                    rating={rating}
                    starRatedColor="blue"
                    changeRating={changeRating}
                    numberOfStars={6}
                    name='rating'
                />
            </ul>
            <ul className="tag">
                <li>TAG:</li>
                <li><a className="color" href>Pink <span>/</span></a></li>
                <li><a className="color" href>T-Shirt <span>/</span></a></li>
                <li><a className="color" href>Girls</a></li>
            </ul>
        </div>
    );
}

export default Rate;