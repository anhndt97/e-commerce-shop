import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

function UpdateUser() {
    let params = useParams();
    const [getInput, setInput] = useState({
        pass:"",
    });
    const [getErr,setErr] = useState({})
    const [getFile,setFile] = useState("")
    const [getAvatar,setAvatar] = useState("")
    const [getUser, setUser] = useState({
        nameuser: "",
        email: "",
        phone: "",
        address: "",
        pass:""
    });
    function handleUserInputFile(e){
        const file = e.target.files
        let render = new FileReader();
        render.onload = (e) =>{
            setAvatar(e.target.result)
            setFile(file[0])
        }
        render.readAsDataURL(file[0]);
    }
    const handleInput = (e) =>{
        const nameInput = e.target.name;
        const value = e.target.value;
        setUser(state=>({...state,[nameInput]: value}));
    }
    useEffect(() => {
        let local = localStorage.getItem("Login")
        let local1 = localStorage.getItem("Password")
        if(local){
            local = JSON.parse(local)
        }
        if(local1){
            local1 = JSON.parse(local1)
        }
        setUser(
            {
                nameuser:local.name,
                email:local.email,
                phone:local.phone,
                address:local.address,
                pass:local1,
                files:local.avatar,
                level:local.level,
                id:local.id
            }
        )
        setInput({
            pass:local1
        })
    },[])
    
    function hanldeForm(e){
        e.preventDefault();
        const img = ["jpeg","jpg","png"]
        const regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        let errorsSubmit ={};
        let flag = true;
        if(getUser.name == ""){
            errorsSubmit.name = "vui long nhap ten"
            flag = false;
        }
        if(getUser.phone == ""){
            errorsSubmit.phone = "vui long nhap phone"
            flag = false;
        }
        if(getUser.address == ""){
            errorsSubmit.address = "vui long nhap dia chi"
            flag = false;
        }
        if(getFile){
            let nameFiles = getFile.type.split("image/")
            if(getFile.size > 1024*1024){
                errorsSubmit.file = "anh lon hon 1mb"
                flag = false;
            }else if(!img.includes(nameFiles[1])){
                errorsSubmit.file = "anh khong dung dinh dang"
                flag = false;
            }
        }

        if(!flag){
            setErr(errorsSubmit);
        }else{
            let localLogin = localStorage.getItem("Login")
            let localToken = localStorage.getItem("Token")
            if (localLogin && localToken) {
                localLogin = JSON.parse(localLogin)
                localToken = JSON.parse(localToken)
            }
            let url = 'http://localhost:8080/laravel/public/api/user/update/'+ localLogin.id;
            let accessToken = localToken.token;
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            
            const formData = new FormData();
            console.log(getUser.level)
            formData.append('phone', getUser.phone);
            formData.append('address', getUser.address);
            formData.append('name', getUser.nameuser);
            formData.append('email', getUser.email);
            formData.append('password', getUser.pass ? getUser.pass : getInput.pass);
            formData.append('avatar',getAvatar ? getAvatar : localLogin.avatar);
            formData.append('level',getUser.level);
            formData.append('id',getUser.id);
            axios.post(url, formData, config)
                .then(response => {
                    console.log(response)
                    alert("Updated")
                });     
        }
    }
    function renderErrors(){
        if(Object.keys(getErr).length > 0){
            return Object.keys(getErr).map((key,index)=>{
                return(
                    <li key={index}>{getErr[key]}</li>
                )
            })
        }
    }
    return (
            <section id="form">{/*form*/}
                            <div className="signup-form">{/*sign up form*/}
                                <h2>Update User</h2>
                                <form encType="multipart/form-data" onSubmit={hanldeForm}>
                                {renderErrors()}
                                    <input type="text" name="nameuser" value={getUser.nameuser} onChange={handleInput} />
                                    <input type="text"  name="email" value={getUser.email}/>
                                    <input type="current-password" name="pass" value={getUser.pass} onChange={handleInput}/>
                                    <input type="text" name="phone" value={getUser.phone} onChange={handleInput}/>
                                    <input type="text" name="address" value={getUser.address} onChange={handleInput}/>
                                    <input type="file" name="files" placeholder="Avatar" value={getUser.avatar} onChange={handleUserInputFile}/>
                                    <button type="submit" className="btn btn-default">Update</button>
                                </form>
                            </div>{/*/sign up form*/}
            </section>
    );
}

export default UpdateUser;