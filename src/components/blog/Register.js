import axios from "axios";
import { useState } from "react";

function Register() {
    const [getInput, setInput] = useState({
        name:"",
        email:"",
        pass:"",
        phone:"",
        address:"",
        level:"",
    });
    const [getErr,setErr] = useState({})
    const [getFile,setFile] = useState("")
    const [getAvatar,setAvatar] = useState("")
    function handleUserInputFile(e){
        const file = e.target.files
        let render = new FileReader();
        render.onload = (e) =>{
            setAvatar(e.target.result)
            setFile(file[0])
        }
        render.readAsDataURL(file[0]);
    }
    const handleInput = (e) =>{
        const nameInput = e.target.name;
        const value = e.target.value;
        setInput(state=>({...state,[nameInput]: value}));
    }

    function hanldeForm(e){
        e.preventDefault();
        const img = ["jpeg","jpg","png"]
        const regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        let errorsSubmit ={};
        let flag = true;
        if(getInput.name == ""){
            errorsSubmit.name = "vui long nhap ten"
            flag = false;
        }
        if(getInput.email == ""){
            errorsSubmit.email = "vui long nhap email";
            flag = false;
        }else if(!regex.test(getInput.email)){
            errorsSubmit.email = "sai dinh dang, vd(abc@gmail.com.vn)"
            flag = false;
        }
        if(getInput.pass == ""){
            errorsSubmit.pass = "vui long nhap pass"
            flag = false;
        }
        if(getInput.phone == ""){
            errorsSubmit.phone = "vui long nhap phone"
            flag = false;
        }
        if(getInput.address == ""){
            errorsSubmit.address = "vui long nhap dia chi"
            flag = false;
        }
        if(getFile == ""){
            errorsSubmit.file = "chon file"
            flag = false;
        }else{
            let nameFiles = getFile.type.split("image/")
            if(getFile.size > 1024*1024){
                errorsSubmit.file = "anh lon hon 1mb"
                flag = false;
            }else if(!img.includes(nameFiles[1])){
                errorsSubmit.file = "anh khong dung dinh dang"
                flag = false;
            }
        if(getInput.level ==""){
            errorsSubmit.level = "vui long nhap level"
            flag = false;
        }}
        
        if(!flag){
            setErr(errorsSubmit);
        }else{
            const data ={
                name: getInput.name,
                email: getInput.email,
                password: getInput.pass,
                phone: getInput.phone,
                address: getInput.address,
                level: 0,
                avatar: getAvatar
            }
            setErr({})
            const success = {}
            axios.post("http://localhost:8080/laravel/public/api/register",data)
            .then(res=>{
                if(res.data.errors){
                    setErr(res.data.errors)
                }else{
                    alert("Register success!")
                }
            })
        }
    }

    function renderErrors(){
        if(Object.keys(getErr).length > 0){
            return Object.keys(getErr).map((key,index)=>{
                return(
                    <li key={index}>{getErr[key]}</li>
                )
            })
        }
    }

    return (
        <div>
            <section id="form">{/*form*/}
                <div className="container">
                    <div className="row">
                        <div className="col-sm-4">
                            <div className="signup-form">{/*sign up form*/}
                                <h2>New User Signup!</h2>
                                <form encType="multipart/form-data" onSubmit={hanldeForm}>
                                    {renderErrors()}
                                    <input type="text"  name="name"  placeholder="Name" onChange={handleInput} />
                                    <input type="text"  name="email"  placeholder="Email Address" onChange={handleInput} />
                                    <input type="current-password" name="pass"  placeholder="Password" onChange={handleInput} />
                                    <input type="text"  name="phone"  placeholder="Phone" onChange={handleInput} />
                                    <input type="text"  name="address"  placeholder="Address" onChange={handleInput} />
                                    <input type="file"  name="files"  placeholder="Avatar" onChange={handleUserInputFile} />
                                    <input type="text"  name="level"  placeholder="Level" onChange={handleInput} />
                                    <button type="submit" className="btn btn-default">Signup</button>
                                </form>
                            </div>{/*/sign up form*/}
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
}

export default Register;