import StarRatings from 'react-star-ratings';
function Foo() {
    const [rating, setRating] = usestate(0)
    function changeRating(newRating, name) {
        setRating(newRating)
    }
    return (
        <StarRatings
            rating={rating}
            starRatedColor="blue"
            changeRating={changeRating}
            numberOfStars={6}
            name='rating'
        />
    );
}