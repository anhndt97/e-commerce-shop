import UserContext from "./UserContext";
import { useReducer } from "react";
function Provider({children}){
    const [state, dispatch] = useReducer();
    return(
        <UserContext.Provider value={[state, dispatch]}>
            {children}
        </UserContext.Provider>
    );
}
export default Provider;