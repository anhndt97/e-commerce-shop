import axios from "axios";
import { createContext, useEffect, useState } from "react";

export const UserContext = createContext({});
export const UserProvider = ({children}) =>{
    let tong = 0
    let objcha = {}
    let product = localStorage.getItem("Cart")
    if (product) {
        product = JSON.parse(product)
    }
    const [getData, setData] = useState([])
    const [getCart, setCart] = useState([])
    useEffect(() => {
        let url = 'http://localhost:8080/laravel/public/api/product/cart';
        axios.post(url, product)
            .then(response => {
                setData(response.data.data)
                setCart(response.data.data.length)
            });
    }, [])
    return(
        <UserContext.Provider value={{getCart}}>
        {children}
    </UserContext.Provider>
    ) 
}

